app.factory("InventorySystem", function ($resource,$window) {

    var InventorySystem = function (client) {

        var loudOut = $resource('json/'+client+'.json');

        this.features = loudOut.query();

    };
    
    InventorySystem.prototype.size = function (frog) {
        frog.size = $window.frog_size;
    };
    
    InventorySystem.prototype.position = function (frog) {
        frog.x = $window.frog_x;
        frog.y = $window.frog_y;
    }
    
    return InventorySystem;
});
