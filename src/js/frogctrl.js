app.controller("frogCtrl", function ($scope, InventorySystem, $window, $location) {
    var $pond_name = $location.search()['pond_name'];;
    /* new object interface */
    var inventory = new InventorySystem($pond_name);

    /* bind to frogs html */
    $scope.frogs = inventory.features;
    
    $scope.capture_width = function(frog) 
    {
        inventory.size(frog);
    }
    
    $scope.capture_position = function(frog) 
    {
        inventory.position(frog);
    }
    
    
});
