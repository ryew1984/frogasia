<!---
Add your comments / notes and thoughts to this doc

<<<<<<< HEAD
Seeing how it's a system to manage the frogs in a pond, I'm recommending an automated system that utilizes image recognition to automate the process of keeping track of the frogs.

Through this simplified demo, am using the color yellow to track the yellow frogs (on a proper system we could apply markers to the frogs or use machine learning to recognize the frogs themselves). 

Currently it can measure the frog size or track the movement of the frogs.

Ideally a high resolution camera would be used and we can segment the 

An enhanced system could track:
- life and death of the frog by tracking if there's no movement for a certain time, we can alert the user to check whether the frog is sick/dead
- mating activities by tracking if two frogs visually overlap each other or are in constant vicinity of each other... e.g. if the frogs are always far apart, that could be a sign of incompatibility
- pond condition can be tracked using color (if the frogs are tracked instead through markers or image recognition), the pond environment would be all white, as the pond deteriorate, the white fades and the system can alert the user when the purity of the whiteness is contaminated

Am using AngularJS + a tracking framework. A factory method is used to decouple the interface from the logic. Ideally all the frog data would be stored and retrieved from a database.

Time to complete this task : 2 Days

Demo here :
https://youtu.be/m6fdInhqmPU

=======
>>>>>>> 548806ccc35cd1130f8f3ca8b9681f55dc743818
Any special instructions to running your code?

Load it up on a localhost, it should work without any dependencies

-->
